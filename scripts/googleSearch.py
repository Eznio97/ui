import os
import requests
from bs4 import BeautifulSoup
import time
import random


def get_result_page(inputData, location=False):
    temp = [0.05, 0.06, 0.07, 0.08, 0.1, 0.03]
    time.sleep(random.choice(temp))
    query = inputData
    query = str(query).replace(' ', '+') + '&num=10'
    url = "https://google.com/search?q=" + str(query)
    if not location:
        try:
            payload = {}
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36',
                'Cookie': 'CGIC=IgMqLyo; NID=211=PtASERRzXGc0SiItOP5rLgy_z5826Jf0rD17AyC9xG'
                          '-NqCAkTlp5PU9vZmtj3IH49M44ROD7nAn2ZUg7HzKiBSbyOyq0ZuNfIFjLS_ViZ3eJ2AwHhDJvsz2jIFqIerCd_DfKFFaMoquNRXbwsPHRw8xkrSuxEN1uwRlAN-CmSFs '
            }
            page = requests.request("GET", url, headers=headers, data=payload)
            return page
        except:
            print('Query Search From Google Failed')
            return '<html>Query Search Error</html>'


def get_result(query):
    page = get_result_page(query)
    if page:
        soup = BeautifulSoup(page.text, 'html5lib')
        results = soup.find_all("div", class_="yuRUbf")
        linkList = []
        for item in results:
            link = item.find('a').get('href')
            linkList.append(link)
        if len(linkList) > 0:
            tempData = linkList
        else:
            tempData = []
            print('empty')
        return tempData


links = get_result('Scarlette Focusrite')
print(links)
